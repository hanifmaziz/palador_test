﻿using Newtonsoft.Json;
using Palador_Organization.Models;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Palador_Organization.Services
{
	public class Employee : IEmployee
	{
		private readonly IHostingEnvironment _environment;
		public void FirstInitialize()
		{
			using (var context = new ApiContext())
			{
				var rootPath = _environment.ContentRootPath;
				var fullPath = Path.Combine(rootPath, "organization-tree.json");
				var jsonData = System.IO.File.ReadAllText(fullPath);
				var employees = JsonConvert.DeserializeObject<List<EmployeeModel>>(jsonData);
				context.EmployeeContext.AddRange(employees);
				context.SaveChanges();
			}
		}
		public Employee(IHostingEnvironment environment)
		{
			_environment = environment;
			using (var context = new ApiContext())
			{
				var list = context.EmployeeContext.ToList();
				if (list.Count ==0)
				{
					FirstInitialize();
				}
			}
		}

		public List<EmployeeModel> GetEmployees()
		{
			using (var context = new ApiContext())
			{
				var list = context.EmployeeContext.ToList();
				return list;
			}
		}
	}
}
