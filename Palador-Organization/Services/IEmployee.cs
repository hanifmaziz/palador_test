﻿using Palador_Organization.Models;

namespace Palador_Organization.Services
{
    public interface IEmployee
    {
		List<EmployeeModel> GetEmployees();
    }
}
