﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Palador_Organization.Models;
using Palador_Organization.Services;
using System;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Palador_Organization.Controllers
{
	[ApiController]
	public class EmployeeController : ControllerBase
	{
		private readonly IEmployee _employee;
		public EmployeeController(IEmployee employee)
		{
			_employee = employee;
		}

		[HttpGet("employees")]
		public IActionResult getEmployees()
		{
			var Employees = _employee.GetEmployees();
			return Ok(Employees);
		}
		[HttpGet("employees/{employeeid}")]
		public IActionResult getByEmployeeId([FromRoute] int employeeid, bool? includeReportingTree)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			var Employees = _employee.GetEmployees();

			if (includeReportingTree == true)
			{
				List<EmployeeModel> listEmployee = new List<EmployeeModel>();
				Employees = Employees.OrderBy(x => x.managerId).ToList();
				List<EmployeeModel> ParentEmployee = Employees.Where(x => x.employeeId == employeeid).ToList(); // fetching the parent employee  
				foreach (EmployeeModel e in ParentEmployee)
				{
					EmployeeModel es = new EmployeeModel
					{
						employeeId = e.employeeId,
						name = e.name,
						managerId = e.managerId,
						status = e.status,
						directReports = BindEmployee(e.employeeId, Employees)
					};
					listEmployee.Add(es);
				}
				return Ok(listEmployee);

			}
			List<EmployeeModel> employee = Employees.Where(i => i.employeeId == employeeid).ToList();


			if (employee != null)
			{
				return Ok(employee);
			}
			return NotFound("Employee not found");
		}
		[HttpPost("employees")]
		public IActionResult addEmployee([FromBody] EmployeeModel newEmployee)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			using (var context = new ApiContext())
			{
				context.EmployeeContext.AddRange(newEmployee);
				context.SaveChanges();
			}
			return Ok("Employee has been added");
		}
		[HttpDelete("employees/{employeeid}")]
		public IActionResult deleteEmployee([FromRoute]int employeeid)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest("Invalid employee Id supplied");
			}
			using (var context = new ApiContext())
			{
				try
				{
					context.EmployeeContext.RemoveRange(new EmployeeModel() { employeeId = employeeid });
					context.SaveChanges();
					return Ok("Employee Deleted");
				}
				catch (Exception ex)
				{
					return NotFound("Employee Not Found");
				}
			}
		}
		[HttpPut("employees/{employeeid}")]
		public IActionResult updateEmployee([FromRoute] int employeeid)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest("nvalid Employee supplied");
			}
			using (var context = new ApiContext())
			{
				try
				{
					context.EmployeeContext.UpdateRange(new EmployeeModel() { employeeId = employeeid, status = "Active" });
					context.SaveChanges();
					return Ok("Employee Updated");
				}
				catch (Exception ex)
				{
					return NotFound("Employee not found");
				}
			}
		}

		private List<EmployeeModel> BindEmployee(int EmpId, List<EmployeeModel> emp)
		{
			List<EmployeeModel> listEmployee = new List<EmployeeModel>();
			foreach (EmployeeModel e in emp.Where(x => x.managerId == EmpId))
			{
				EmployeeModel es = new EmployeeModel
				{
					employeeId = e.employeeId,
					name = e.name,
					managerId = e.managerId,
					status = e.status,
					directReports = BindEmployee(e.employeeId, emp)
				};
				listEmployee.Add(es);
			}
			return listEmployee;
		}
	}

}
