﻿using System.ComponentModel.DataAnnotations;

namespace Palador_Organization.Models
{
	public class EmployeeModel
	{
		[Key]
		public int employeeId { get; set; }
		public string? name { get; set; }
		public int managerId { get; set; }
		public string? status { get; set; } = "Inactive";
		public List<EmployeeModel>? directReports { get; set; }
	}
}
