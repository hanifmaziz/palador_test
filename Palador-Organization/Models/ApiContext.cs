﻿using Microsoft.EntityFrameworkCore;
namespace Palador_Organization.Models
{
	public class ApiContext : DbContext
	{
		protected override void OnConfiguring
	   (DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseInMemoryDatabase(databaseName: "EmployeeDb");
		}
		public DbSet<EmployeeModel> EmployeeContext { get; set; }
	}
}
